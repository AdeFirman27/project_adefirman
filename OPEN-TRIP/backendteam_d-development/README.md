# BackendTeam_D

# Application Name: OPEN-TRIP

# UPDATED

```
-on 28 June 2021
-on 29 June 2021
-on 30 June 2021
-on 02 July 2021
-on 03 July 2021
-on 07 July 2021
```

# DEVELOPER: Backend Team-D Final Project -----Glints Academy

```
$ Ade Firmansyah
$ Fardian Thofani
$ Mohamad Nouval Abdel Alkaff
$ Muhammad Ary Widodo
```

---

# How to run

```bash
$ cd server
$ npm i
$ npm start
```

# API List

TRAVELLER API

| Routes | EndPoint                            | Description                                            |
| ------ | ----------------------------------- | ------------------------------------------------------ |
| POST   | /api/ot/travel/regis                | Register user as a Traveler (input requirement:        |
|        |                                     | username, email, and password)                         |
| POST   | /api/ot/user/login                  | Get Token & Login Traveler with (email and password )  |
| GET    | /api/ot/travel/all                  | Get all Traveler (ADMIN only)                          |
| GET    | /api/ot/travel                      | Get Traveler by id                                     |
| PUT    | /api/ot/travel/upd/                 | Update traveler by id (id and email should be matched) |
| DELETE | /api/ot/travel/del/                 | Delete traveler by id (ADMIN and Traveler)             |

```
POST
/api/ot/travel/regis
username: {string}, form-encoded
email: {string}, form-encoded
password: {string}, form-encoded


POST
/api/ot/user/login
(GENERATE TOKEN)
email: {string}, form-encoded
password: {string}, form-encoded


GET
/api/ot/travel/all
(TOKEN ADMIN)


GET
/api/ot/travel
(TOKEN ADMIN OR TRAVELLER)
id: {integer}, query


PUT
/api/ot/travel/upd
!! id and email should be matched !!
!! email cannot be updated !!
(TOKEN TRAVELLER)
id: {integer}, query
email: {string}, form-encoded
username: {string}, form-encoded
password: {string}, form-encoded


DELETE
/api/ot/travel/del
(TOKEN ADMIN OR TRAVELLER)
id: {integer}, query
```


HOST API

| Routes | EndPoint                            | Description                                            |
| ------ | ----------------------------------- | ------------------------------------------------------ |
| POST   | /api/ot/host/regis                  | Register user as a Host (username, email, password,    |
|        |                                     | no_phone, alamat, no_ktp, pict_ktp, selfie_with_ktp,   |
|        |                                     | bank, no_rekening, tabungan_pict)                      |
| POST   | /api/ot/user/login                  | Get Token & Login Host with (email and password)       |
| GET    | /api/ot/host/all                    | Get all Host (ADMIN only)                              |
| GET    | /api/ot/host                        | Get Host by id                                         |
| PUT    | /api/ot/host/upd                    | Update Host by id (id and email should be matched)     |
| DELETE | /api/ot/host/del                    | Delete Host by id (ADMIN and Host)                     |

```
POST
/api/ot/host/regis
username: {string}, form
email: {string}, form
password: {string}, form
no_phone: {string}, form
alamat: {string}, form
no_ktp: {string}, form
pict_ktp: {image}, form --> Cannot Empty
selfie_with_ktp: {image}, form --> Cannot Empty
bank: {string}, form
no_rekening: {string}, form
tabungan_pict: {image}, form --> Cannot Empty


POST
/api/ot/user/login
(GENERATE TOKEN)
email: {string}, form-encoded
password: {string}, form-encoded


GET
/api/ot/host/all
(TOKEN ADMIN)


GET
/api/ot/host
(TOKEN ADMIN OR HOST)
id: {integer}, query


PUT
/api/ot/host/upd
!! id and email should be matched !!
!! email cannot be updated !!
(TOKEN HOST)
id: {integer}, query
username: {string}, form-encoded
email: {string}, form-encoded
password: {string}, form-encoded
no_phone: {string}, form-encoded
alamat: {string}, form-encoded
no_ktp: {string}, form-encoded
bank: {string}, form-encoded
no_rekening: {string}, form-encoded

DELETE
/api/ot/host/del
(TOKEN ADMIN OR HOST)
id: {integer}, query
```


ADMIN API

| Routes | EndPoint                            | Description                                            |
| ------ | ----------------------------------- | ------------------------------------------------------ |
| POST   | /api/ot/admin/regis                 | Register Admin (email and password)                    |
| POST   | /api/ot/admin/login                 | Get Token & Login Admin with (email and password)      |
| GET    | /api/ot/admin/all                   | Get all Admin (ADMIN only)                             |
| GET    | /api/ot/admin                       | Get Admin by id (ADMIN only)                           |
| PUT    | /api/ot/admin/upd                   | Update Admin by id (id and email should be matched)    |
| DELETE | /api/ot/admin/del                   | Delete Admin by id (ADMIN only)                        |

```
POST
/api/ot/admin/regis
full_name: {string}, form-encoded
email: {string}, form-encoded
password: {string}, form-encoded


POST
/api/ot/admin/login 
(GENERATE TOKEN)
email: {string}, form-encoded
password: {string}, form-encoded


GET
/api/ot/admin/all
(TOKEN ADMIN)


GET
/api/ot/admin
(TOKEN ADMIN)
id: {integer}, query


PUT
/api/ot/admin/upd
!! id and email should be matched !!
!! email cannot be updated !!
(TOKEN TRAVELLER)
id: {integer}, query
email: {string}, form-encoded
username: {string}, form-encoded
password: {string}, form-encoded


DELETE
/api/ot/admin/del
(TOKEN ADMIN)
id: {integer}, query
```


ROLE API (FOR BACKEND DEV ONLY)

| Routes | EndPoint                            | Description                                            |
| ------ | ----------------------------------- | ------------------------------------------------------ |
| POST   | /api/ot/role/crt                    | Create role of users (Admin only)                      |
| GET    | /api/ot/role/all                    | Get all roles of users (Admin only)                    |

```
POST (FOR BACKEND DEV ONLY)
/api/ot/role/crt 
role_type: {string}, form-encoded
(TOKEN ADMIN)


GET (FOR BACKEND DEV ONLY)
/api/ot/role/all
(TOKEN ADMIN)
```


CATEGORY API

| Routes | EndPoint                            | Description                                                     |
| ------ | ----------------------------------- | --------------------------------------------------------------- |
| POST   | /api/ot/category/crt                | Create category (ADMIN only)                                    |
| GET    | /api/ot/category/all                | Get all categories                                              |
| GET    | /api/ot/category                    | Get a category by id                                            |
| PUT    | /api/ot/category/upd                | Update a category by (id, query) (Admin only)                   |
| DELETE | /api/ot/category/del                | Delete a category by (id, query) (Admin only)                   |
| GET    | /api/ot/category/search             | Get a category by filter (query,page, cat1-cat5) + (pagination) |
```
POST
/api/ot/category/crt
(TOKEN ADMIN)
category_name: {string}, form-encoded


GET
/api/ot/category/all


GET
/api/ot/category
id: {integer}, query


PUT
/api/ot/category/upd
(TOKEN ADMIN)
id: {integer}, query
category_name: {string}, form-encoded


DELETE
/api/ot/category/del
(TOKEN ADMIN)
id: {integer}, query

GET
/api/ot/category/search
page: {integer}, query
limit: {integer}, query 
cat1: {string}, query
cat2: {string}, query
cat3: {string}, query
cat4: {string}, query
cat5: {string}, query

```


DESTINATION API

| Routes | EndPoint                            | Description                                                 |
| ------ | ----------------------------------- | ----------------------------------------------------------- |
| POST   | /api/ot/destination/crt             | Create destination/trip (destination_name, location,        |
|        |                                     | min_kuota_sk, destination_pict, tipe_perjalanan, date,      |
|        |                                     | latitude, longitude, kuota, duration, harga) + (Pagination) |
| GET    | /api/ot/destination/all             | Get all destinations/trip                                   |
| GET    | /api/ot/destination                 | Get a destination/trip by id (input id)                     |
| PUT    | /api/ot/destination/upd             | Update a destination by id (Host)                           |
| DELETE | /api/ot/destination/del             | Delete a destination by id (Host and Admin)                 |
| GET    | /api/ot/destination/name/search     | Get all destination with the spesific name (input name      |
|        |                                     | of the destination/trip) + (Pagination)                     |
| GET    | /api/ot/destination/budget/search   | Get all destination with budget that is desireable in       |
|        |                                     | with in the range (input max and min of the budget)         | 
|        |                                     | + (Pagination)   									         |     
| GET    | /api/ot/destination/date/search     | Get all destinatiion with spesific date (input date of      |
|        |                                     | the destination) + (Pagination)                             |
| GET    | /api/ot/destination/duration/search | filter destinatiion by duration + (Pagination)              |
| GET    | /api/ot/destination/plan            | Get all destinatiion include relation with days & plan      |
| GET    | /api/ot/destination/sk              | Get all destinatiion include relation with syarat           |
|        |                                     | ketentuan & detail syarat ketentuan                         |
```
POST
/api/ot/destination/crt 
(TOKEN HOST)
categories_id: {integer}, form
host_id: {integer}, form
galeri_id: {integer}, form
trip_name: {string}, form
harga: {string}, form
date_1: {string}, form (format MM/DD/YYYY) --> Cannot Empty
duration_trip_1, {integer}, form
date_2: {string}, form (format MM/DD/YYYY) --> Fill 0 or Can Be Empty
duration_trip_2, {integer}, form
date_3: {string}, form (format MM/DD/YYYY) --> Fill 0 or Can Be Empty
duration_trip_3, {integer}, form
kuota: {string}, form
pickup_spot_1: {string}, form
koordinat_1: {string}, form
thumbnail_pict: {image}, form --> Cannot Empty
pict_1: {image}, form --> Cannot Empty
pict_2: {image}, form --> Cannot Empty
pict_3: {image}, form --> Cannot Empty
pict_4: {image}, form --> Cannot Empty


GET
/api/ot/category/all


GET
/api/ot/category
id: {integer}, query


GET
/api/ot/destination/name/search
trip_name: {string}, query
page: {integer}, query
limit: {integer}, query


GET
/api/ot/destination/budget/search
min: {integer}, query
max: {integer}, query
page: {integer}, query
limit: {integer}, query


GET
/api/ot/destination/date/search
start: {string}, query (format MM/DD/YYYY) --> Cannot Empty
end: {string}, query (format MM/DD/YYYY) --> Cannot Empty
page: {integer}, query
limit: {integer}, query


PUT
/api/ot/destination/upd
(TOKEN HOST)
id: {integer}, query
categories_id: {integer}, form
host_id: {integer}, form
galeri_id: {integer}, form
trip_name: {string}, form
harga: {string}, form
date_1: {string}, form (format MM/DD/YYYY) --> Cannot Empty
duration_trip_1, {integer}, form
date_2: {string}, form (format MM/DD/YYYY) --> Fill 0 or Can Be Empty
duration_trip_2, {integer}, form
date_3: {string}, form (format MM/DD/YYYY) --> Fill 0 or Can Be Empty
duration_trip_3, {integer}, form
kuota: {string}, form
pickup_spot_1: {string}, form
koordinat_1: {string}, form
thumbnail_pict: {image}, form --> Cannot Empty
pict_1: {image}, form --> Cannot Empty
pict_2: {image}, form --> Cannot Empty
pict_3: {image}, form --> Cannot Empty
pict_4: {image}, form --> Cannot Empty


DELETE
/api/ot/destination/del
(TOKEN ADMIN OR HOST)
id: {integer}, query

GET
/api/ot/destination/duration/search
minim: {integer}, query
maxim: {integer}, query
limit: {integer}, query
page: {integer}, query

GET
/api/ot/destinatiion/plan

GET
/api/ot/destinatiion/sk

```

GALERI API

| Routes | EndPoint                            | Description                                            |
| ------ | ----------------------------------- | ------------------------------------------------------ |
| GET    | /api/ot/pict/all                    | Get All Galeri Photo of destinatiion                   |
| GET    | /api/ot/pict                        | Get spesific galeri photo of destination by galeri ID  |

```

GET
/api/ot/pict/all

GET
/api/ot/pict
id: {integer}, query
