/**
 * Read file with Synchronous or Asynchronous, you can choose one method
 * Create new data to file dummyData.js
 * Don't replace existing data, only added the new data
 * new data ['mango','avocado','durian','guava']
 */

// write your code here

// Panggil data di file lain
const fs = require('fs');


const read = fs.readFileSync('dummyData.js', 'utf8');
var test = read.replace(']', '');

// New data yang akan di masukan ke dummyData.js
const newDataUpdate = "'mango', 'avocado', 'durian', 'guava'";
const write = () => {
    try {
        fs.writeFileSync('./dummyData.js', test + `${newDataUpdate}]` );
            console.log('Berhasil Menambah Data!')
        }catch (err) {
            console.log('Error');
    }
}

write()
