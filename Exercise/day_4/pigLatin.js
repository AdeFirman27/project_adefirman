function pigLatin(words) {
  const vocalWord = ['a', 'i', 'u', 'e', 'o'];
  
  let vowelIndex = 0


  if (vocalWord.includes(words[0])){
    return words + "ay"
  }else{
    for (let i = 0; i < words.length; i++) {
      if (vocalWord.includes(words[i])) {
        vowelIndex = words.indexOf(words[i]);
        break;
      }
    }
    return words.slice(vowelIndex) + words.slice(0, vowelIndex) + "ay";
  }
}

// Driver code
console.log(pigLatin('food')) // ---> oodfay
console.log(pigLatin('snap')) // ---> apsnay
console.log(pigLatin('guide')) // ---> uidegay
console.log(pigLatin('beli makanan')) // ---> elibay akananmay
console.log(pigLatin('apel')) // ---> apel