/**
 * Mampu menggabungkan konsep Conditional dan Iteration dalam kasus sederhana
 * Buat sebuah function yang bisa menerima parameter yang bisa melengkapi for loop.
 * function yang dibuat hanya bisa menerima tipe data number
 * Buat pseudocode nya terlebih dahulu 
 */

//Menentukan bilangan ganjil & genap
for (var i = 1; i <= 10; i++){
  if(i%2 == 0){
    console.log( i + " adalah bilangan ganjil")
  }else{
    console.log(i + " adalah bilangan genap")
  }
}

// kelipatan 3
for ( var i = 1; i <= 10; i++){
  if (i % 3 === 0) {
    console.log("Kelipatan 3")
  }else{
    console.log(i)
  }
}

// faktor prima
const primeFactor = (number, hasil = []) => {
  if(typeof number !== 'number') return console.log('Type data bukan number');
  if (number < 2) return

  let totalFactor = 0;

  for (let i = 2; i <= number; i++){
    if(number % i !== 0) continue

    for (let j = 1; j <= i; j++){
      if ((i % j) === 0) totalFactor++
    }

    if (totalFactor > 2) continue

    number /= i
    hasil.push(i)
    totalFactor = 0
  }

  return hasil
}

console.log(primeFactor(50));


