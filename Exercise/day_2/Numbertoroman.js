function convertNumberToRoman(num) {
    const romanMap = ["I","IV","V","IX","X","XL","L","C","CD","D","CM","M"];
    const numeralMap = [1,4,5,9,10,40,50,100,400,500,900,1000];
    
    let length = romanMap.length;
    let result = "";

    while(num > 0) {
       const currentIndex = length-1;
       const count = parseInt(num / numeralMap[currentIndex]);  
    
       for(let i=0; i< count; i++) {
            result = result + romanMap[currentIndex];
       }

       num = num % numeralMap[currentIndex];
       length--;
    }

    return result;
}

console.log(convertNumberToRoman(30));
